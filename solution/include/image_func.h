#ifndef IMAGE_FUNC_H
#define IMAGE_FUNC_H
#include "../include/utils.h"
#include <stddef.h>
#include <stdint.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};
struct pixel {
    uint8_t b, g, r;
};

struct image *create_image(uint64_t width, uint64_t height);

void destroy_image(struct image *image);

size_t get_padding(uint64_t width);

#endif //IMAGE_FUNC_H
