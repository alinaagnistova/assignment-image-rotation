#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

enum error_desc {
    INVALID_DATA = 0,
    INVALID_OPEN_START_FILE,
    INVALID_ROTATING,
    INVALID_OPEN_END_FILE,
    INVALID_WRITING_TO_FILE,
    INVALID_READING_FROM_FILE

};

int divides(uint64_t a, int b);

void error(enum error_desc error);

#endif //UTILS_H
