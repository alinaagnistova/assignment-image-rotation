#ifndef IMAGE_TRANSFORM_H
#define IMAGE_TRANSFORM_H

#include "image_func.h"

struct image *rotate(const struct image *source);

#endif //IMAGE_TRANSFORM_H
