#include "../include/image_func.h"
#include "../include/image_transform.h"
#include "../include/io_format.h"
#include <stdio.h>


void destroy_all(struct image *img1, struct image *img2) {
    destroy_image(img1);
    destroy_image(img2);
}

int main(int argc, char **argv) {
    if (argc != 3) {
        error(INVALID_DATA);
    }
    FILE *in = fopen(argv[1], "rb");
    if (in == NULL) {
        error(INVALID_OPEN_START_FILE);
    }
    struct image *img = NULL;
    enum read_status r_result = from_bmp(in, &img);
    struct image *r_img = NULL;
    if (r_result == READ_OK) {
        r_img = rotate(img);
        if (r_img == NULL) {
            destroy_image(img);
            error(INVALID_ROTATING);
        }
        FILE *out = fopen(argv[2], "wb");
        if (out == NULL) {
            destroy_all(img, r_img);
            error(INVALID_OPEN_END_FILE);
        }
        enum write_status w_result = to_bmp(out, r_img);
        fclose(out);
        if (w_result != WRITE_OK) {
            destroy_all(img, r_img);
            error(INVALID_WRITING_TO_FILE);
        }
    } else {
        error(INVALID_READING_FROM_FILE);
    }
    destroy_all(img, r_img);

    return 0;
}
