#include "../include/image_func.h"
#include <malloc.h>
#include <stdint.h>
struct image *create_image(uint64_t width, uint64_t height) {
    struct image *image = (struct image *) malloc(sizeof(struct image));
    if (image == NULL) {
        return NULL;
    } else{
        image->height = height;
        image->width = width;
        image->data = malloc(width * height * sizeof(struct pixel));
        if (image->data == NULL){
            free(image);
            return NULL;
        }
        return image;
    }
}

void destroy_image(struct image *image) {
    if (image != NULL) {
        free(image->data);
        free(image);
    }
}

size_t get_padding(uint64_t width) {
    int multiple = 4;
    size_t result;
    if (divides(width, multiple)) {
        result = 0;
    } else {
        size_t remainder = (width * sizeof(struct pixel)) % multiple;
        result = multiple - remainder;
    }
    return result;
}
