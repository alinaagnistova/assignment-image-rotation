#include "../include/image_transform.h"

struct image *rotate(const struct image *source) {
    struct image *r_image = create_image(source->height, source->width);
    if (r_image != NULL)
        for (uint64_t y = 0; y < source->height; y++)
            for (uint64_t x = 0; x < source->width; x++)
                r_image->data[r_image->width - 1 - y + x * r_image->width] = source->data[y * source->width + x];
    return r_image;
}

