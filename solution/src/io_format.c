#include  "../include/io_format.h"
#define BMP_BIT_COUNT 24
#define BMP_BF_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_BI_COMPRESSION 0
#define BMP_BI_X_PELS_PER_METER 0
#define BMP_BI_Y_PELS_PER_METER 0
#define BMP_BI_CRL_USED 0
#define BMP_BI_CLR_IMPORTANT 0


enum read_status from_bmp(FILE *in, struct image **image) {
    struct bmp_header image_header = {0};
    if (fread(&image_header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (image_header.biBitCount != BMP_BIT_COUNT) {
        return READ_INVALID_BITS;
    }
    *image = create_image(image_header.biWidth, image_header.biHeight);
    size_t padding = get_padding((*image)->width);
    for (uint32_t y = 0; y < image_header.biHeight; y++) {
        if (fread(&(*image)->data[y * image_header.biWidth], sizeof(struct pixel), image_header.biWidth, in) != image_header.biWidth){
            return READ_ERROR;
        }
        if (fseek(in, (long) padding, SEEK_CUR) != 0){
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *image) {
    size_t padding = get_padding(image->width);
    struct bmp_header image_header = (struct bmp_header) {
            .bfType = 0x4D42,
            .bfileSize = (image->width * sizeof(struct pixel) + padding) * image->height + sizeof(struct bmp_header),
            .bfReserved = BMP_BF_RESERVED,
            .biSize = BMP_SIZE,
            .biHeight = image->height,
            .bOffBits = sizeof(struct bmp_header),
            .biBitCount = BMP_BIT_COUNT,
            .biWidth = image->width,
            .biPlanes = BMP_PLANES,
            .biCompression = BMP_BI_COMPRESSION,
            .biSizeImage = (image->width * sizeof(struct pixel) + padding) * image->height,
            .biXPelsPerMeter = BMP_BI_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_BI_Y_PELS_PER_METER,
            .biClrUsed = BMP_BI_CRL_USED,
            .biClrImportant = BMP_BI_CLR_IMPORTANT
    };
    if (fwrite(&image_header, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    for (uint32_t y = 0; y < image->height; y++) {
        if (fwrite(&image->data[y * image->width], sizeof(struct pixel), image->width, out) != image->width)
            return WRITE_ERROR;

        for (size_t p = 0; p < padding; p++)
            if (fputc(0, out) == EOF)
                return WRITE_ERROR;
    }
    return WRITE_OK;
}



