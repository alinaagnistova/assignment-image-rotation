#include  "../include/utils.h"
#include <stdio.h>
#include <stdlib.h>

int divides(uint64_t a, int b) {
    return a % b == 0;
}

void error(enum error_desc error) {
    char *error_desc[] = {
            "Неккоректные входные данные",
            "Ошибка при открытии исходного файла",
            "Ошибка при повороте изображения",
            "Ошибка при открытии конечного файла",
            "Ошибка при записи перевернутого изображения",
            "Ошибка при чтении исходного изображения"
    };
    printf("%s", error_desc[error]);
    exit(1);
}

